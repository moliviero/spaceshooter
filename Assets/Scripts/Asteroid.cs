﻿using UnityEngine;
using System.Collections;
using Utils;
using System;

namespace SS
{
    [RequireComponent(typeof(Rigidbody))]
    public class Asteroid : MonoBehaviour
    {
        public enum Status { Destroyed, OutOfScreen, Hit}

        public event Action<Asteroid, Status> StatusChanged = delegate { };

        [SerializeField]
        private float m_speed;
        public float Speed { get { return m_speed; } set { m_speed = value; } }

        [SerializeField]
        private Vector3 m_dir;
        public Vector3 Dir { get { return m_dir; } set { m_dir = value; } }


        [SerializeField]
        private float m_rotSpeed;

        [SerializeField]
        private float m_defaultLife;
       
        [SerializeField]
        private float m_life;

        private Rigidbody m_body;
        private Vector3 m_rotAxis;

        void Awake()
        {
            m_body = GetComponent<Rigidbody>();
            m_rotAxis = Vector3.up;//new Vector3(1, 3f, 2f);
            m_rotAxis.Normalize();
        }
        void Update()
        {
            transform.Rotate(m_rotAxis, m_rotSpeed * Time.deltaTime, Space.Self);
        }
        void OnEnable()
        {
            m_life = m_defaultLife;
            m_body.velocity = m_dir * m_speed;
        }
        void OnValidate()
        {
            if (m_dir.magnitude <= 0.01f)
                m_dir = -Vector3.forward;

            m_dir.Normalize();
        }

        void OnTriggerEnter(Collider other)
        {
            switch (other.gameObject.layer)
            {
                case (int)LayerNames.PlayerBullet:
                {
                    Missile m = other.gameObject.GetComponent<Missile>();

                    m_life -= m.Damage;
                    if (m_life <= 0f)
                    {
                        StatusChanged(this, Status.Destroyed);
                      
                    }
                    break;
                }
                case (int)LayerNames.Cleaner:
                {
                    StatusChanged(this, Status.OutOfScreen);
                    break;
                }
            }

        }

        public int Score { get { return 25; } }
    }
}