﻿using UnityEngine;
using System;
using System.Collections;

namespace SS
{
    public class RotatorBullets : MonoBehaviour
    {
        public enum Status { COMPLETED }
        public event Action<RotatorBullets, Status> StatusChanged = delegate { };

        [SerializeField]
        private Transform[] m_bullets;
        [SerializeField]
        private float m_speed;
        [SerializeField]
        private float m_rotSpeed;

        private int m_activeBullets = 0;

        public void Spawn()
        {
            for (int i = 0; i < m_bullets.Length; ++i)
            {
                m_bullets[i].gameObject.SetActive(true);
            }
            m_activeBullets = m_bullets.Length;
         
            StartCoroutine(Shoot());

        }
        void OnTriggerEnter(Collider other)
        {
            if (!gameObject.activeInHierarchy)
                return;
            m_activeBullets--;

            if (m_activeBullets == 0)
            {
                StatusChanged(this, Status.COMPLETED);
            }
        }

        private IEnumerator Shoot()
        {

            Vector3 dir = transform.forward;

            while (m_activeBullets > 0)
            {
                transform.Translate(dir * Time.deltaTime * m_speed, Space.World);

                transform.Rotate(Vector3.up, Time.deltaTime * m_rotSpeed, Space.Self);
                yield return null;
            }
          
        }
    }
}