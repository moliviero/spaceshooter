﻿using UnityEngine;
using System.Collections;
using System;
using Utils;

namespace SS
{
    public class LevelManager : MonoBehaviour
    {
        [Serializable]
        private class Level
        {
            public int maxAsteroids;
            public float spawnRate;
            public float duration;

            public int maxBullets4;
            public int maxRotators;

        }
        private ShipController m_shipController;

        [SerializeField]
        private Level[] m_levels;

     //   private int m_currLevel;
        private int CurrentLevel
        {
            get { return m_gameStatus.Level; }
            set { m_gameStatus.Level = value; }
        }
        private float m_playTime = 0f;

        [SerializeField]
        private Transform[] m_asteroidsSpawnPoints;
        [SerializeField]
        private Transform[] m_bullets4SpawnPoints;
        [SerializeField]
        private Transform[] m_rotatorsSpawnPoints;

        private int m_activeAsteroids;
        private int m_activeBullets4;
        private int m_activeRotators;

        private IEnumerator m_spawner;
        private GameStatus m_gameStatus;
        public GameStatus GameStatus { get { return m_gameStatus; } }

        void Awake()
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            m_shipController = player.GetComponent<ShipController>();

            m_shipController.StatusChanged += OnPlayerStatusChanged;
            m_shipController.Damaged += OnPlayerDamaged;
           
            m_gameStatus = new GameStatus();
            m_gameStatus.Life = (int)m_shipController.Life;
        }

        private void OnPlayerDamaged(ShipController arg1, float arg2)
        {
            m_gameStatus.Life = (int)arg2;
        }

        void Start()
        {
            m_spawner = Spawner();
            StartCoroutine(m_spawner);
        }

        void Update()
        {
            if (m_gameStatus.CurrentStatus != SS.GameStatus.Status.PLAY)
                return;
            m_playTime += Time.deltaTime;

            int levelIndex = m_gameStatus.Level - 1;

            if (m_levels[levelIndex].duration <= m_playTime)
            { 
                //finished curr level
                m_gameStatus.Level = m_gameStatus.Level + 1;

                if (m_gameStatus.Level > m_levels.Length - 1)
                {
                    Time.timeScale = 0f;

                    m_gameStatus.CurrentStatus = SS.GameStatus.Status.COMPLETED;
          
                    Debug.Log("GAME COMPLETED");
                    StopCoroutine(m_spawner);
                }
            }
        }
        IEnumerator Spawner()
        {
            while (true)
            {
                int levelIndex = m_gameStatus.Level - 1;
                Level currLevel = m_levels[levelIndex];
                SpawnAsteroids(currLevel);
                SpawnBullets4(currLevel);
                SpawnRotators(currLevel);

                yield return new WaitForSeconds(currLevel.spawnRate);
            }
        }
        private void SpawnAsteroids(Level currLev)
        {
            if (currLev.maxAsteroids > m_activeAsteroids)
            {
                int spawnP = UnityEngine.Random.Range(0, m_asteroidsSpawnPoints.Length - 1);
                Transform spawnPos = m_asteroidsSpawnPoints[spawnP];

                GameObject asteroid = ObjectPool.Instance.Instantiate("Asteroid");

                asteroid.transform.position = spawnPos.position;
                asteroid.transform.rotation = spawnPos.rotation;

                ++m_activeAsteroids;

                Asteroid a = asteroid.GetComponent<Asteroid>();

                a.StatusChanged += OnAsteroidEvent;
            }
            // 
        }
        private void SpawnRotators(Level currLev)
        {
            
            if (currLev.maxRotators > m_activeRotators)
            {
                int spawnP = UnityEngine.Random.Range(0, m_rotatorsSpawnPoints.Length - 1);
                Transform spawnPos = m_rotatorsSpawnPoints[spawnP];

                GameObject rotator = ObjectPool.Instance.Instantiate("Rotator");

                rotator.transform.position = spawnPos.position;
                rotator.transform.rotation = spawnPos.rotation;

                ++m_activeRotators;

                RotatorBullets a = rotator.GetComponent<RotatorBullets>();

                a.StatusChanged += OnRotatorEvent;
                a.Spawn();
    
            }
        }



        private void SpawnBullets4(Level currLev)
        {
            if (currLev.maxBullets4 > m_activeBullets4)
            {
                int spawnP = UnityEngine.Random.Range(0, m_bullets4SpawnPoints.Length - 1);
                Transform spawnPos = m_bullets4SpawnPoints[spawnP];

                GameObject bullet4 = ObjectPool.Instance.Instantiate("Bullet4");

                bullet4.transform.position = spawnPos.position;
                bullet4.transform.rotation = spawnPos.rotation;

                ++m_activeBullets4;

                Bullet4 a = bullet4.GetComponent<Bullet4>();
                
                a.StatusChanged += OnBullet4Event;
                a.Spawn();
            }

        }

        private void OnBullet4Event(Bullet4 bullet, Bullet4.Status status)
        {
            switch (status)
            {
                case Bullet4.Status.COMPLETED:
                {
                    bullet.StatusChanged -= OnBullet4Event;
                    ObjectPool.Instance.Destroy(bullet.gameObject, "Bullet4");
                    m_activeBullets4--;
                    break;
                }
                
            }
        }
        private void OnRotatorEvent(RotatorBullets rotator, RotatorBullets.Status status)
        {
            switch (status)
            {
                case RotatorBullets.Status.COMPLETED:
                {
                    rotator.StatusChanged -= OnRotatorEvent;
                    ObjectPool.Instance.Destroy(rotator.gameObject, "Rotator");
                    m_activeRotators--;
                    break;
                }
            }
        }
        

        private void OnAsteroidEvent(Asteroid asteroid, Asteroid.Status status)
        {
            switch (status)
            {
                case Asteroid.Status.Destroyed:
                {
                    ObjectPool.Instance.Destroy(asteroid.gameObject, "Asteroid");
                    asteroid.StatusChanged -= OnAsteroidEvent;
                    --m_activeAsteroids;

                    m_gameStatus.Score = m_gameStatus.Score + asteroid.Score;
                    break;
                }
                case Asteroid.Status.OutOfScreen:
                {
                    ObjectPool.Instance.Destroy(asteroid.gameObject, "Asteroid");
                    asteroid.StatusChanged -= OnAsteroidEvent;
                    --m_activeAsteroids;

                    break;
                }
            }
        }

        private void OnPlayerStatusChanged()
        {
            if (m_shipController.CurrentStatus == ShipController.Status.DEAD)
            {
                Time.timeScale = 0f;

                m_gameStatus.CurrentStatus = SS.GameStatus.Status.DEAD;
                Debug.Log("player dead");
            }
        }

        internal void Restart()
        {
            Time.timeScale = 1f;
            
            Application.LoadLevel(0);
        }

        internal void ExitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit()
#endif
        }
    }
}