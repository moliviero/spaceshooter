﻿using UnityEngine;
using System;

namespace SS
{
    [RequireComponent(typeof(Rigidbody))]
    public class ShipController : MonoBehaviour
    {
        public enum Status { ALIVE, DEAD}
        public event Action StatusChanged = delegate { };
        public event Action<ShipController,float> Damaged = delegate { };
        
        [SerializeField]
        private float m_maxSpeed;
        [SerializeField]
        private float m_maxAcc;
        [SerializeField]
        private float m_rotationSpeed;

        [SerializeField]
        private float m_maxX;
        [SerializeField]
        private float m_maxZ;

        [SerializeField]
        private Transform m_model;
        [SerializeField]
        [Range(0f,45f)]
        private float m_rollAngle;

        
        private Rigidbody m_body;
        private Quaternion leftRot;
        private Quaternion rightRot;

        [SerializeField]
        public float m_life = 100f;
        public float Life { get { return m_life; } }

        private Status m_currStatus = Status.ALIVE;
        public Status CurrentStatus 
        {
            get { return m_currStatus; }
            set 
            {
                m_currStatus = value;
                StatusChanged();
            }
        }
        void Awake()
        {
            m_body = GetComponent<Rigidbody>();

            leftRot = Quaternion.AngleAxis(m_rollAngle, Vector3.forward);
            rightRot = Quaternion.AngleAxis(-m_rollAngle, Vector3.forward);

        
        }
        void Update()
        {
            float inputHorizontal = Input.GetAxis("Horizontal");
            float inputVertical = Input.GetAxis("Vertical");
 
            UpdateRot(inputHorizontal);

           
        }
        void FixedUpdate()
        {
            float inputHorizontal = Input.GetAxis("Horizontal");
            float inputVertical = Input.GetAxis("Vertical");
 
            UpdatePos(inputHorizontal, inputVertical);
        
        }

       
      
        private void UpdateRot(float dir)
        { 
            Quaternion dest =  dir > 0f ? rightRot : leftRot;

            dest = dir == 0f ? Quaternion.identity : dest;

            m_model.rotation = Quaternion.RotateTowards(m_model.rotation, dest, m_rotationSpeed * Time.deltaTime);

        }
        private void UpdatePos(float inputHorizontal, float inputVertical)
        {
            Vector3 pos = m_body.position;

           
            pos.x += inputHorizontal * m_maxAcc * Time.deltaTime;
            pos.z += inputVertical * m_maxAcc * Time.deltaTime;

            pos.x = Mathf.Clamp(pos.x, -m_maxX, m_maxX);
            pos.z = Mathf.Clamp(pos.z, 0f, m_maxZ);

            m_body.MovePosition(pos);
        
        }

        void OnTriggerEnter(Collider other)
        {
            switch (other.gameObject.layer)
            {
                case (int)LayerNames.Asteroid:
                {
                    CurrentStatus = Status.DEAD;
                    break;
                }
                case (int)LayerNames.EnemyBullet:
                {
                    Damage damage = other.GetComponent<Damage>();
                    m_life -= damage.Value;

                    Damaged(this, m_life);
                    if (m_life <= 0f)
                        CurrentStatus = Status.DEAD;

                    break;
                }
            }
            
        }

        
    }
}