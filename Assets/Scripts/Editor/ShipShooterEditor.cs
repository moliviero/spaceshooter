﻿using UnityEngine;
using UnityEditor;

namespace SS
{
    [CustomEditor(typeof(ShipShooter))]
    public class Spa : Editor
    {
        ShipShooter ss;

        void OnEnable()
        {
            ss = target as ShipShooter;
        }
        void OnSceneGUI()
        {
            Vector3 cannonPos_WS = ss.transform.position + Vector3.forward * ss.Offset;
            EditorGUI.BeginChangeCheck();

            Handles.color = Color.yellow;
      
            float newPos = Handles.ScaleValueHandle(ss.Offset,
                        cannonPos_WS,
                        Quaternion.identity,
                        6,
                        Handles.ArrowCap,
                        HandleUtility.GetHandleSize(cannonPos_WS));      	
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(ss, "change cannon offset");
                ss.Offset = newPos;//cannonPos_WS.z - ss.transform.position.z;
            }
        }
    }
}