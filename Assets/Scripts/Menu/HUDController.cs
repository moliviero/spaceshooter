﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


namespace SS
{
    public class HUDController : MonoBehaviour {


        [SerializeField]
        private GameObject m_completePanel;
        [SerializeField]
        private GameObject m_deadPanel;
 
        [SerializeField]
        private Text m_scoreLabel;
        [SerializeField]
        private Text m_lifeLabel;
        [SerializeField]
        private Text m_levelLabel;

        private GameStatus m_gameStatus;
        private LevelManager lm;


        void Awake()
        {
            m_completePanel.SetActive(false);
            m_deadPanel.SetActive(false);
        }
        void Start()
        {
            lm = GameObject.FindObjectOfType<LevelManager>();

            BindGameStatus(lm.GameStatus);
        }
        public void BindGameStatus(GameStatus status)
        {
            if (m_gameStatus != null)
            {
                m_gameStatus.ScoreUpdated -= OnScoreUpdated;
                m_gameStatus.LifeUpdated -= OnEnergyUpdated;
                m_gameStatus.LevelUpdated -= OnLevelUpdated;
                m_gameStatus.StatusChanged -= OnStatusChanged;

            }
            m_gameStatus = status;
            m_gameStatus.ScoreUpdated += OnScoreUpdated;
            m_gameStatus.LifeUpdated += OnEnergyUpdated;
            m_gameStatus.LevelUpdated += OnLevelUpdated;
            m_gameStatus.StatusChanged += OnStatusChanged;

            m_lifeLabel.text = "ENERGY: " + m_gameStatus.Life;
            m_scoreLabel.text = "SCORE: " + m_gameStatus.Score;
            m_levelLabel.text = "LEVEL: " + m_gameStatus.Level;

        }

        private void OnStatusChanged(GameStatus arg1, GameStatus.Status status)
        {
            switch (status)
            {
                case GameStatus.Status.COMPLETED:
                {
                    m_completePanel.SetActive(true);
                    break;
                }
                case GameStatus.Status.DEAD:
                {
                    m_deadPanel.SetActive(true);
                    break;
                }
            }
        }

        private void OnLevelUpdated(GameStatus arg1, int arg2)
        {
            m_levelLabel.text = "LEVEL: " + arg2;
        }

        private void OnEnergyUpdated(GameStatus arg1, float arg2)
        {
            m_lifeLabel.text = "ENERGY: " + arg2;
        }

        private void OnScoreUpdated(GameStatus arg1, int arg2)
        {
            m_scoreLabel.text = "SCORE: " + arg2;
        }

        public void OnRestartButtonPressed()
        {
            lm.Restart();
        }
        public void OnExitButtonPressed()
        {
            lm.ExitGame();
        }
    }
}