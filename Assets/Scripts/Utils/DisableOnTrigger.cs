﻿using UnityEngine;
using System.Collections;

namespace Utils
{
    public class DisableOnTrigger : MonoBehaviour
    {

        void OnTriggerEnter()
        {
            gameObject.SetActive(false);
        }
    }
}