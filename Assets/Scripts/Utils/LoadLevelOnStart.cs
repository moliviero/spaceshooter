﻿using UnityEngine;
using System.Collections;

public class LoadLevelOnStart : MonoBehaviour {

    public int level;
	// Use this for initialization
	void Start () {
        Application.LoadLevel(level);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
