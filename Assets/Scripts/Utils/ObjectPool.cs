﻿using UnityEngine;
using System;
using System.Collections.Generic;

namespace Utils
{
    public class ObjectPool : MonoBehaviour
    {
        [Serializable]
        private struct PooledObject
        {
            public GameObject go;
            public string id; //TODO optimize with hash
            public int count;
        }

        [SerializeField]
        private PooledObject[] m_objList;

        private Dictionary<string, List<GameObject>> m_pool;
        private Dictionary<string, List<GameObject>> m_usedPool;
        private static ObjectPool s_instance;

        public static ObjectPool Instance {
            get { return s_instance; }
            set { s_instance = value; 
            }
        }
        void Awake()
        {
            s_instance = this;
            m_pool = new Dictionary<string, List<GameObject>>();
            m_usedPool = new Dictionary<string, List<GameObject>>();
    
            foreach (PooledObject po in m_objList)
            {
                List<GameObject> poolList = new List<GameObject>();

                for (int i = 0; i < po.count; ++i)
                {
                  
                    GameObject instance = GameObject.Instantiate(po.go);
                    instance.transform.parent = transform;

                    instance.name = po.go.name + "_" + i;
                    instance.SetActive(false);

                    poolList.Add(instance);
                }
                m_pool[po.id] = poolList;
                m_usedPool[po.id] = new List<GameObject>();
            }
        }

        public GameObject Instantiate(string id)
        {
            List<GameObject> available = m_pool[id];

            if (available.Count <= 0)
                Debug.LogError("No more instance for object " + id);

            int lastIndex = available.Count - 1;
            GameObject ret = available[lastIndex];
            available.RemoveAt(lastIndex);

            ret.transform.parent = null;
            m_usedPool[id].Add(ret);

            ret.SetActive(true);
            return ret;
        }

        public void Destroy(GameObject go, string id)
        {
            List<GameObject> used = m_usedPool[id];

            int instanceId = go.GetInstanceID();
            GameObject obj = used.Find(g => g.GetInstanceID() == instanceId);

            if (obj == null)
                Debug.LogError("Object " + go.name + " of type " + id + " has never been instantiated");

            used.Remove(obj);

            obj.transform.parent = transform;
            obj.SetActive(false);
        
            m_pool[id].Add(obj);


        }
        
    }
}