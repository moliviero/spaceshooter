﻿using UnityEngine;
using System;


namespace SS
{
    public class GameStatus 
    {
        public enum Status { PLAY, PAUSE, COMPLETED, DEAD}

        public event Action<GameStatus, int> ScoreUpdated = delegate { };
        public event Action<GameStatus, float> LifeUpdated = delegate { };
        public event Action<GameStatus, int> LevelUpdated = delegate { };
        public event Action<GameStatus, Status> StatusChanged = delegate { };

        private Status m_status;
        public Status CurrentStatus
        {
            get { return m_status; }
            set { m_status = value; StatusChanged(this,m_status); }
        }
        private int m_score;
        public int Score
        {
            get { return m_score; }
            set
            {
                m_score = value;
                ScoreUpdated(this, m_score);
            }
        }


        private int m_level;
        public int Level
        {
            get { return m_level; }
            set
            {
                m_level = value;
                LevelUpdated(this, m_level);
            }
        }


        private int m_life;
        public int Life
        {
            get { return m_life; }
            set
            {
                m_life = value;
                LifeUpdated(this, m_life);
            }
        }

        public GameStatus()
        {
            m_level = 1;
        }

    }
}