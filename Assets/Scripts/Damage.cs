﻿using UnityEngine;
using System.Collections;


namespace SS
{
    public class Damage : MonoBehaviour
    {
        [SerializeField]
        private float m_damage;
        public float Value { get { return m_damage; } }
    }
}