﻿using UnityEngine;
using System.Collections;
using Utils;

namespace SS
{
    [RequireComponent(typeof(Rigidbody))]
    public class Missile : MonoBehaviour
    {

      private Rigidbody m_body;

        [SerializeField]
        private float speed;
        [SerializeField]
        private float m_damage;
        public float Damage { get { return m_damage; } set { m_damage = value; } }
        void Awake()
        {
            m_body = GetComponent<Rigidbody>();
        }

        void OnEnable()
        {
            Shoot();
        }
        public void Shoot()
        { 
       
            m_body.velocity = transform.forward * speed; 
        }


        void OnTriggerEnter(Collider other)
        {

           
            ObjectPool.Instance.Destroy(gameObject, "Missile");
            
        }
    }
}