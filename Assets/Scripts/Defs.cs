﻿
namespace SS
{
    public enum Weapon { LaserSingle, LaserDouble }
    public enum LayerNames { PlayerBullet = 10, EnemyBullet = 11, Cleaner = 12, Asteroid = 13 }
}
