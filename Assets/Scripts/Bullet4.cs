﻿using UnityEngine;
using System.Collections;
using System;


namespace SS
{
    public class Bullet4 : MonoBehaviour
    {
        public enum Status { COMPLETED}
        public event Action<Bullet4, Status> StatusChanged = delegate { };
        
        //[SerializeField]
        //private float m_damage;

        [SerializeField]
        private Transform[] m_bullets;

        [SerializeField]
        private float m_explosionTime;

        [SerializeField]
        private float m_speedBeforeExplosion;
        [SerializeField]
        private float m_speedAfterExplosion;


        private IEnumerator m_behaviour;

        private int m_activeBullets = 0;

        public void Spawn()
        {
            m_activeBullets = 1;
            m_behaviour = Shoot();

            m_bullets[0].localPosition = Vector3.zero;

            m_bullets[0].gameObject.SetActive(true);

            for (int i = 1; i < m_bullets.Length; ++i)
            {
                m_bullets[i].gameObject.SetActive(false);
            }

            StartCoroutine(m_behaviour);
        
        }
        void OnEnable()
        {
        }

        void OnDisable()
        {
            for (int i = 0; i < m_bullets.Length; ++i)
            {
                m_bullets[i].localPosition = Vector3.zero;
            }

        }
        private IEnumerator Shoot()
        {
            float t = 0f;

            while (t < m_explosionTime && m_activeBullets > 0)
            {
                m_bullets[0].transform.Translate(transform.forward * m_speedBeforeExplosion * Time.deltaTime, Space.World);

                t += Time.deltaTime;
                yield return null;
            }
           
            m_bullets[1].gameObject.SetActive(true);
            m_bullets[1].position = m_bullets[0].position;
            m_bullets[1].rotation = Quaternion.LookRotation(Vector3.right);

            m_bullets[2].gameObject.SetActive(true);
            m_bullets[2].position = m_bullets[0].position;
            m_bullets[2].rotation = Quaternion.LookRotation(-Vector3.right);

            m_bullets[3].gameObject.SetActive(true);
            m_bullets[3].position = m_bullets[0].position;
            m_bullets[3].rotation = Quaternion.LookRotation(Vector3.forward);

            m_activeBullets += 3;

            while (m_activeBullets > 0)
            {
                for (int i = 0; i < m_bullets.Length; ++i)
                { 
                    Transform bullet = m_bullets[i];
                    if (bullet.gameObject.activeInHierarchy)
                    {
                        bullet.Translate(bullet.forward * m_speedAfterExplosion * Time.deltaTime, Space.World);
                    }

                }
                yield return null;
            }

        }

        void OnTriggerEnter(Collider other)
        {
            if (!gameObject.activeInHierarchy)
                return;
            m_activeBullets--;
        
            if (m_activeBullets == 0)
            {
                StatusChanged(this,Status.COMPLETED);
            }
        }
    }
}