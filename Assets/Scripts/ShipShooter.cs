﻿using UnityEngine;
using System.Collections;
using Utils;

namespace SS
{
    public class ShipShooter : MonoBehaviour
    {

        [SerializeField]
        private float m_offset;
        public float Offset { get { return m_offset; } set { m_offset = value; } }

        [SerializeField]
        private float m_shootDelay;

        private float m_timeFromLastShoot;

        private ObjectPool m_pool;

        void Awake()
        {
            m_timeFromLastShoot = float.MaxValue;
            m_pool = GameObject.FindGameObjectWithTag("ObjectPool").GetComponent<ObjectPool>();
        }
        void Update()
        {
            Fire();
            m_timeFromLastShoot += Time.deltaTime;
        }

        private void Fire()
        {

            if (Input.GetButton("Fire") && m_timeFromLastShoot > m_shootDelay)
            {
                m_timeFromLastShoot = 0f;
                Shoot();
            }
        }
        private void Shoot()
        {
            GameObject missile = m_pool.Instantiate("Missile");

            missile.transform.position = transform.position + Vector3.forward * m_offset;

            
        }

    }
}